//
//  customTextField.swift
//  Afty
//
//  Created by Thijs Kuipers on 22/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit

@IBDesignable
class customTextField: UITextField {
    @IBInspectable var insetX: CGFloat = 0
     @IBInspectable var insetY: CGFloat = 0
    
    // placeholder position
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds , insetX , 0)
    }
    
    // text position
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return CGRectInset(bounds , insetX , 0)
    }
    
    
}
