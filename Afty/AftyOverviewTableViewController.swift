//
//  AftyOverviewTableViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 04/11/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire

class AftyOverviewTableViewController: UITableViewController {
    var afties = [Afty]()
    var selectedEvent: Event?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.navigationBar.barTintColor = UIColor(netHex:0x0f1217)
        navigationController!.navigationBar.backItem?.backBarButtonItem?.tintColor = UIColor(netHex:0xc9582b)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        loadJsonData()
    }
    
    func loadJsonData()
    {
        Alamofire.request(.POST, "http://i261412.iris.fhict.nl/afty/data/afties.php", parameters: ["key": 10, "e": self.selectedEvent!.id]).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                self.afties.removeAll()
                for var i = 0; i < JSON.count; ++i {
                    let timeFormatter = NSDateFormatter()
                    timeFormatter.dateFormat = "HH:mm:ss"
                    let time:NSDate = timeFormatter.dateFromString(JSON[i]["time"] as! String)!
                    let id:Int = Int(JSON[i]["id"] as! String)!
                    let name:String = JSON[i]["name"] as! String
                    let image:String = JSON[i]["image"] as! String
                    let maxguests:Int = Int(JSON[i]["guests_max"] as! String)!
                    let guests:Int = Int(JSON[i]["guests_count"] as! String)!
                    let city:String = JSON[i]["city"] as! String
                    let address:String = JSON[i]["address"] as! String
                    let fromname:String = JSON[i]["fromname"] as! String
                    let newAfty = Afty(id: id, name: name, starttime: time, image: image, maxguests: maxguests, guests: guests, city: city, address: address, fromname: fromname)
                    
                    self.afties.append(newAfty)
                }
                self.tableView.reloadData()
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return afties.count + 1
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        var height: CGFloat
         if(indexPath.row==0){
            height = 193.0
         } else {
            height = 168.0
        }
        return height
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if(indexPath.row==0){
            cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) 
            
            // Configure the cell...
            let image = cell.contentView.viewWithTag(200) as! UIImageView
            loadImage("http://i261412.iris.fhict.nl/afty/img/event/\(selectedEvent!.backdrop)", img: image)
            let lblEvent = cell.contentView.viewWithTag(201) as! UILabel
            let lblCity = cell.contentView.viewWithTag(202) as! UILabel
            let lblAfties = cell.contentView.viewWithTag(203) as! UILabel
            let lblAfter = cell.contentView.viewWithTag(204) as! UILabel
            lblEvent.text = selectedEvent!.name
            lblCity.text = selectedEvent!.city
            lblAfties.text = "\(selectedEvent!.numafties) Afties"
            lblAfter.text = "93 Aftergangers"
            
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            // Configure the cell...
            let currentRow = indexPath.row - 1
            let currentAfty = afties[currentRow]
            let image = cell.contentView.viewWithTag(300) as! UIImageView
            loadImage("http://i261412.iris.fhict.nl/afty/img/afty/\(currentAfty.image)", img: image)
            let lblName = cell.contentView.viewWithTag(301) as! UILabel
            let lblPerson = cell.contentView.viewWithTag(302) as! UILabel
            let lblTime = cell.contentView.viewWithTag(303) as! UILabel
            let lblNum = cell.contentView.viewWithTag(304) as! UILabel
            lblName.text = currentAfty.name
            lblPerson.text = currentAfty.fromname
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "hh:mm:ss"
            let dateString = dateFormatter.stringFromDate(currentAfty.starttime)
            lblTime.text = dateString
            lblNum.text = "\(currentAfty.guests)/\(currentAfty.maxguests)"
            
            let btnAttend = cell.contentView.viewWithTag(305) as! UIButton
            btnAttend.addTarget(self, action: "attendPressed:", forControlEvents: .TouchUpInside)
        }
        // Configure the cell...

        return cell
    }
    
    func attendPressed(sender:UIButton!){
        print("test")
    }
    
    func loadImage(urlString:String, img:UIImageView)
    {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    img.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
            
        }
        
        task.resume()
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let viewController = segue.destinationViewController as? NewAftyViewController {
            viewController.currentEvent = selectedEvent
        }
        
    }

}


