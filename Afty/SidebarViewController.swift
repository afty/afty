//
//  SidebarViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 22/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit

class SidebarViewController: UITableViewController {

    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBAction func btnLogout(sender: AnyObject) {
        let appDomain = NSBundle.mainBundle().bundleIdentifier
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain!)
        self.performSegueWithIdentifier("logout_segue", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let username:String = prefs.valueForKey("USERNAME") as! String
        lblUsername.text = username
        
        loadImage("http://i261412.iris.fhict.nl/afty/img/user/\(prefs.valueForKey("AVATAR") as! String)", img: userAvatar)
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func loadImage(urlString:String, img:UIImageView)
    {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    img.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
            
        }
        
        task.resume()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}
