//
//  Afty.swift
//  Afty
//
//  Created by Thijs Kuipers on 04/11/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import Foundation

class Afty: NSObject{
    var id:Int
    var name:String
    var starttime:NSDate
    var image:String
    var maxguests:Int
    var guests:Int
    var city:String
    var address:String
    var fromname:String
    
    init(id:Int, name:String, starttime:NSDate, image:String, maxguests:Int, guests:Int, city:String, address:String, fromname:String){
        self.id = id
        self.name = name
        self.starttime = starttime
        self.image = image
        self.maxguests = maxguests
        self.guests = guests
        self.city = city
        self.address = address
        self.fromname = fromname
    }
}