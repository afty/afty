//
//  RegisterViewController.swift
//  Afty
//
//  Created by Fhict on 22/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {

    @IBOutlet weak var iconUser: UILabel!
    @IBOutlet weak var iconEmail: UILabel!
    @IBOutlet weak var iconPass: UILabel!
    
    @IBOutlet weak var gebruikersnaam: UITextField!
    @IBOutlet weak var wachtwoord: UITextField!
    @IBOutlet weak var email: UITextField!
    
    

    @IBAction func registreerBtn(sender: UIButton) {
        
        let username:NSString = gebruikersnaam.text! as NSString
        let password:NSString = wachtwoord.text! as NSString
        let emailAddress:NSString = email.text! as NSString
        
        func validateEmail(candidate: String) -> Bool {
            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
            return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluateWithObject(candidate)
        }
        

        
        
        if ( username.isEqualToString("") || password.isEqualToString("") || emailAddress.isEqualToString("") ) {
            
            let alertController = UIAlertController(title: "Registratie Gefaald", message: "Je hebt niet alles ingevuld!", preferredStyle: .Alert)
            
            
            let opnieuwInloggen = UIAlertAction(title: "Probeer Opnieuw", style: .Default) { _ in }
            alertController.addAction(opnieuwInloggen)
            
            self.presentViewController(alertController, animated: true, completion:nil)
        }
        
        else if (validateEmail(emailAddress as String)){
            Alamofire.request(.POST, "http://i261412.iris.fhict.nl/afty/register.php", parameters: ["username": username, "password":password, "email": emailAddress]).responseJSON
                { response in switch response.result {
                case .Success(let JSON):
                    if(JSON["success"] as! Int==0){
                        let message:String = JSON["error_message"] as! String
                        let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                        self.presentViewController(alert, animated: true){}
                    } else {
                        self.performSegueWithIdentifier("goto_login", sender: self)
                    }
                    
                    
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    }
            }
            
        }
        else{
            let alertController = UIAlertController(title: "Login Gefaald", message: "Je hebt geen geldig email ingevoerd!", preferredStyle: .Alert)
            
            
            let opnieuwInloggen = UIAlertAction(title: "Probeer Opnieuw", style: .Default) { _ in }
            alertController.addAction(opnieuwInloggen)
            
            self.presentViewController(alertController, animated: true, completion:nil)

        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        iconUser.setFAIcon(FAType.FAUser, iconSize: 14)
        iconPass.setFAIcon(FAType.FALock, iconSize: 14)
        iconEmail.setFAIcon(FAType.FAAt,  iconSize: 14)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
