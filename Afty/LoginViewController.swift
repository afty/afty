//
//  LoginViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 16/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPwd: UITextField!
    
    @IBOutlet weak var iconUser: UILabel!
    @IBOutlet weak var iconPwd: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        iconUser.setFAIcon(FAType.FAUser, iconSize: 14)
        iconPwd.setFAIcon(FAType.FALock, iconSize: 14)
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let isLoggedIn:Int = prefs.integerForKey("ISLOGGEDIN") as Int
        if (isLoggedIn == 1) {
            self.performSegueWithIdentifier("goto_home", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLogin(sender: AnyObject) {
        let username:String = txtUser.text!
        let password:String = txtPwd.text!
        Alamofire.request(.POST, "http://i261412.iris.fhict.nl/afty/login.php", parameters: ["username": username, "password":password]).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                if(JSON["success"] as! Int==0){
                    let message:String = JSON["error_message"] as! String
                    let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                } else {
                    let uid = JSON["user_id"] as! Int
                    let username = JSON["user_username"] as! String
                    let firstname = JSON["user_firstname"] as! String
                    let lastname = JSON["user_lastname"] as! String
                    let birthday = JSON["user_birthday"] as! String
                    let address = JSON["user_address"] as! String
                    let avatar = JSON["user_avatar"] as! String
                    let city = JSON["user_city"] as! String
                    var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    prefs.setInteger(uid, forKey: "UID")
                    prefs.setObject(username, forKey: "USERNAME")
                    prefs.setObject(firstname, forKey: "FIRSTNAME")
                    prefs.setObject(lastname, forKey: "LASTNAME")
                    prefs.setObject(birthday, forKey: "BIRTHDAY")
                    prefs.setObject(address, forKey: "ADDRESS")
                    prefs.setObject(avatar, forKey: "AVATAR")
                     prefs.setObject(city, forKey: "CITY")
                    prefs.setInteger(1, forKey: "ISLOGGEDIN")
                    prefs.synchronize()
                    self.performSegueWithIdentifier("goto_home", sender: self)
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    

}
