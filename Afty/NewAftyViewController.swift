//
//  NewAftyViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 05/11/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire

class NewAftyViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var currentEvent: Event?
    
    @IBOutlet weak var txtGuests: UITextField!
    @IBOutlet weak var aantalGasten: UIPickerView!
    
    var gastenArray = ["10", "25", "50", "100", "200", "500"]
    var testtt = 1
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var aftyImage: UIImageView!
    
    override func viewDidLoad() {

        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.barTintColor = UIColor(netHex:0x0f1217)
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xc9582b)]
        
        let pickerView = UIPickerView()
        pickerView.delegate = self;
        txtGuests.inputView = pickerView
       
        
        // txtGuests.keyboardType = UIKeyboardType.NumberPad
    
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        
        toolBar.barStyle = UIBarStyle.BlackTranslucent
        
        toolBar.tintColor = UIColor.whiteColor()
        
        toolBar.backgroundColor = UIColor.blackColor()
        
        
        let defaultButton = UIBarButtonItem(title: "Annuleer", style: UIBarButtonItemStyle.Plain, target: self, action: "tappedToolBarBtn:")
        let doneButton = UIBarButtonItem(title: "Klaar", style: UIBarButtonItemStyle.Plain, target: self, action: "donePressed:")
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: self, action: nil)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        label.font = UIFont(name: "Helvetica", size: 12)
        label.backgroundColor = UIColor.clearColor()
        label.textColor = UIColor.whiteColor()
        label.text = "Max. aantal gasten"
        label.textAlignment = NSTextAlignment.Center
        let textBtn = UIBarButtonItem(customView: label)
        toolBar.setItems([defaultButton,flexSpace,textBtn,flexSpace,doneButton], animated: true)
        txtGuests.inputAccessoryView = toolBar
        
    }
    
    
    func donePressed(sender: UIBarButtonItem) {
        txtGuests.resignFirstResponder()
    }
    
    func tappedToolBarBtn(sender: UIBarButtonItem) {
        // txtGuests.text = "123"
         txtGuests.resignFirstResponder()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gastenArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gastenArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtGuests.text = gastenArray[row]
    }
    
    
    
    @IBAction func addAfty(sender: AnyObject) {
        // init paramters Dictionary
        var parameters = [
            "task": "task",
            "variable1": "var"
        ]
        
        // add addtionial parameters
        let uid:String = String(prefs.valueForKey("UID") as! Int)
        parameters["name"] = txtName.text
        parameters["guests"] = txtGuests.text
        parameters["event"] = String(currentEvent!.id)
        parameters["uid"] = uid
        // example image data
        let image = aftyImage.image
        let imageData = UIImagePNGRepresentation(image!)
        
        
        
        // CREATE AND SEND REQUEST ----------
        
        let urlRequest = urlRequestWithComponents("http://i261412.iris.fhict.nl/afty/add-afty.php", parameters: parameters, imageData: imageData!)
        
        Alamofire.upload(urlRequest.0, data: urlRequest.1)
            .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                print("\(totalBytesWritten) / \(totalBytesExpectedToWrite)")
            }
            .responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                if(JSON["success"] as! Int==0){
                    let message:String = JSON["error_message"] as! String
                    let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                } else {
                    self.dismissViewControllerAnimated(true, completion: {})
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    
    
    
    @IBAction func selectImage(sender: AnyObject) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        self.presentViewController(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject]){
            aftyImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            
            self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancelAfty(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: {})
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, imageData:NSData) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData(imageData)
        
        // add parameters
        for (key, value) in parameters {
            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }


}
