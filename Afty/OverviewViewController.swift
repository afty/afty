//
//  OverviewViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 23/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire
class OverviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var events = [Event]()
    
    @IBOutlet weak var tblDemo: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var customView: UIView!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var labelsArray: Array<UILabel> = []
    
    var isAnimating = false
    
    var currentColorIndex = 0
    
    var currentLabelIndex = 0
    
    var timer: NSTimer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        navigationController!.navigationBar.barTintColor = UIColor(netHex:0x0f1217)
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xc9582b)]
        navigationController!.navigationBar.barStyle = UIBarStyle.Black

        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        tblDemo.delegate = self
        tblDemo.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clearColor()
        refreshControl.tintColor = UIColor.clearColor()
        refreshControl.addTarget(self, action: "loadJsonData", forControlEvents: UIControlEvents.ValueChanged)
        tblDemo.addSubview(refreshControl)
        
        loadCustomRefreshContents()
        loadJsonData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: UITableview method implementation
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        // Configure the cell...
        let currentRow = indexPath.row
        let currentEvent = events[currentRow]
        let image = cell.contentView.viewWithTag(100) as! UIImageView
        loadImage("http://i261412.iris.fhict.nl/afty/img/event/\(currentEvent.picture)", img: image)
        let lblEvent = cell.contentView.viewWithTag(101) as! UILabel
        let lblCity = cell.contentView.viewWithTag(102) as! UILabel
        let lblAft = cell.contentView.viewWithTag(103) as! UILabel
        
        lblEvent.text = currentEvent.name
        lblCity.text = currentEvent.city
        lblAft.text = "\(currentEvent.numafties) Afties"
        return cell
    }
    
    func loadJsonData()
    {
        Alamofire.request(.POST, "http://i261412.iris.fhict.nl/afty/data/events.php", parameters: ["key": 10]).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                self.events.removeAll()
                for var i = 0; i < JSON.count; ++i {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let date:NSDate = dateFormatter.dateFromString(JSON[i]["date"] as! String)!
                    let timeFormatter = NSDateFormatter()
                    timeFormatter.dateFormat = "HH:mm:ss"
                    let time:NSDate = timeFormatter.dateFromString(JSON[i]["starttime"] as! String)!
                    let name:String = JSON[i]["name"] as! String
                    let duration:Double = Double(JSON[i]["duration"] as! String)!
                    let picture:String = JSON[i]["picture"] as! String
                    let city:String =  JSON[i]["city"] as! String
                    let address:String =  JSON[i]["address"] as! String
                    let location:String = JSON[i]["location"] as! String
                    let backdrop:String = JSON[i]["backdrop"] as! String
                    let numafties:Int = Int(JSON[i]["numafties"] as! String)!
                    let id:Int = Int(JSON[i]["id"] as! String)!
                    let newEvent = Event(id: id, name: name, date: date, starttime: time , duration: duration, picture: picture, city: city, address: address, location: location, backdrop: backdrop, numafties: numafties)
                    
                    self.events.append(newEvent)
                }
            self.tblDemo.reloadData()
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
    }
    
    func loadImage(urlString:String, img:UIImageView)
    {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    img.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
            
        }
        
        task.resume()
    }

    
    
    
    // MARK: UIScrollView delegate method implementation
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if refreshControl.refreshing {
            if !isAnimating {
                doSomething()
                animateRefreshStep1()
            }
        }
    }
    
    
    // MARK: Custom function implementation
    
    func loadCustomRefreshContents() {
        let refreshContents = NSBundle.mainBundle().loadNibNamed("RefreshContents", owner: self, options: nil)
        
        customView = refreshContents[0] as! UIView
        customView.frame = refreshControl.bounds
        
        for var i=0; i<customView.subviews.count; ++i {
            labelsArray.append(customView.viewWithTag(i + 1) as! UILabel)
        }
        
        refreshControl.addSubview(customView)
    }
    
    
    func animateRefreshStep1() {
        isAnimating = true
        
        UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.labelsArray[self.currentLabelIndex].transform = CGAffineTransformMakeRotation(CGFloat(M_PI_4))
            self.labelsArray[self.currentLabelIndex].textColor = self.getNextColor()
            
            }, completion: { (finished) -> Void in
                
                UIView.animateWithDuration(0.05, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
                    self.labelsArray[self.currentLabelIndex].transform = CGAffineTransformIdentity
                    self.labelsArray[self.currentLabelIndex].textColor = UIColor.whiteColor()
                    
                    }, completion: { (finished) -> Void in
                        ++self.currentLabelIndex
                        
                        if self.currentLabelIndex < self.labelsArray.count {
                            self.animateRefreshStep1()
                        }
                        else {
                            self.animateRefreshStep2()
                        }
                })
        })
    }
    
    
    func animateRefreshStep2() {
        UIView.animateWithDuration(0.35, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            self.labelsArray[0].transform = CGAffineTransformMakeScale(1.5, 1.5)
            self.labelsArray[1].transform = CGAffineTransformMakeScale(1.5, 1.5)
            self.labelsArray[2].transform = CGAffineTransformMakeScale(1.5, 1.5)
            self.labelsArray[3].transform = CGAffineTransformMakeScale(1.5, 1.5)
            
            }, completion: { (finished) -> Void in
                UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
                    self.labelsArray[0].transform = CGAffineTransformIdentity
                    self.labelsArray[1].transform = CGAffineTransformIdentity
                    self.labelsArray[2].transform = CGAffineTransformIdentity
                    self.labelsArray[3].transform = CGAffineTransformIdentity
                    
                    }, completion: { (finished) -> Void in
                        if self.refreshControl.refreshing {
                            self.currentLabelIndex = 0
                            self.animateRefreshStep1()
                        }
                        else {
                            self.isAnimating = false
                            self.currentLabelIndex = 0
                            for var i=0; i<self.labelsArray.count; ++i {
                                self.labelsArray[i].textColor = UIColor.whiteColor()
                                self.labelsArray[i].transform = CGAffineTransformIdentity
                            }
                        }
                })
        })
    }
    
    
    func getNextColor() -> UIColor {
        var colorsArray: Array<UIColor> = [UIColor.magentaColor(), UIColor.brownColor(), UIColor.yellowColor(), UIColor.redColor(), UIColor.greenColor(), UIColor.blueColor(), UIColor.orangeColor()]
        
        if currentColorIndex == colorsArray.count {
            currentColorIndex = 0
        }
        
        let returnColor = colorsArray[currentColorIndex]
        ++currentColorIndex
        
        return returnColor
    }
    
    
    func doSomething() {
        timer = NSTimer.scheduledTimerWithTimeInterval(4.0, target: self, selector: "endOfWork", userInfo: nil, repeats: true)
    }
    
    
    func endOfWork() {
        refreshControl.endRefreshing()
        
        timer.invalidate()
        timer = nil
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let selectedRow = self.tblDemo.indexPathForSelectedRow
        let selectedEvent = events[selectedRow!.row]
        let controller = segue.destinationViewController as! AftyOverviewTableViewController
        controller.selectedEvent = selectedEvent
    }
    


}


