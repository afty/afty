//
//  Event.swift
//  Afty
//
//  Created by Thijs Kuipers on 23/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import Foundation

class Event: NSObject{
    var id:Int
    var name:String
    var date:NSDate
    var starttime:NSDate
    var duration:Double
    var picture:String
    var city:String
    var address:String
    var location:String
    var backdrop:String
    var numafties:Int
    
    init(id:Int, name:String, date:NSDate, starttime:NSDate, duration:Double, picture:String, city:String, address:String, location:String, backdrop:String, numafties:Int){
        self.id = id
        self.name = name
        self.date = date
        self.starttime = starttime
        self.duration = duration
        self.picture = picture
        self.city = city
        self.address = address
        self.location = location
        self.backdrop = backdrop
        self.numafties = numafties
    }
}
