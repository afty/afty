//
//  ProfileViewController.swift
//  Afty
//
//  Created by Thijs Kuipers on 23/10/15.
//  Copyright © 2015 Thijs Kuipers. All rights reserved.
//

import UIKit
import Alamofire

class ProfileViewController: UITableViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
        var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var txtFirstname: UITextField!
    @IBOutlet weak var txtLastname: UITextField!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    //icon labels
    @IBOutlet weak var iconFirstname: UILabel!
    @IBOutlet weak var iconAchternaam: UILabel!
    @IBOutlet weak var iconAchternaam2: UILabel!
    @IBOutlet weak var iconGeboortedatum: UILabel!
    @IBOutlet weak var iconAdres: UILabel!
    @IBOutlet weak var iconStad: UILabel!
    
    @IBAction func textFieldEditing(sender: UITextField) {
        
        let inputView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 240))
        
        var datePickerView  : UIDatePicker = UIDatePicker(frame: CGRectMake(0, 40, 0, 0))
        datePickerView.datePickerMode = UIDatePickerMode.Date
        inputView.addSubview(datePickerView) // add date picker to UIView
        
        let doneButton = UIButton(frame: CGRectMake((self.view.frame.size.width/2) - (100/2), 0, 100, 50))
        doneButton.setTitle("Klaar", forState: UIControlState.Normal)
        doneButton.setTitle("Klaar", forState: UIControlState.Highlighted)
        doneButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        doneButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
        
        inputView.addSubview(doneButton) // add Button to UIView
        
        doneButton.addTarget(self, action: "doneButton:", forControlEvents: UIControlEvents.TouchUpInside) // set button click event
        
        sender.inputView = inputView
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        datePickerValueChanged(datePickerView)
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        txtBirthday.text = dateFormatter.stringFromDate(sender.date)
        
    }
    
    func doneButton(sender:UIButton)
    {
        txtBirthday.resignFirstResponder() // To resign the inputView on clicking done.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController!.navigationBar.barTintColor = UIColor(netHex:0x0f1217)
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor(netHex:0xc9582b)]
        navigationController!.navigationBar.barStyle = UIBarStyle.Black
        
        // icons
        iconFirstname.setFAIcon(FAType.FAUser, iconSize: 14)
        iconAchternaam2.setFAIcon(FAType.FAUser, iconSize: 14)
        iconAchternaam.setFAIcon(FAType.FAUser, iconSize: 10)
        iconGeboortedatum.setFAIcon(FAType.FACalendarO, iconSize: 14)
        iconAdres.setFAIcon(FAType.FAHome, iconSize: 14)
        iconStad.setFAIcon(FAType.FAMapMarker, iconSize: 14)
        

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        fillData()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillData(){

        let firstname:String = prefs.valueForKey("FIRSTNAME") as! String
        let lastname:String = prefs.valueForKey("LASTNAME") as! String
        let birthday:String = prefs.valueForKey("BIRTHDAY") as! String
        let address:String = prefs.valueForKey("ADDRESS") as! String
        let city:String = prefs.valueForKey("CITY") as! String
        txtFirstname.text = firstname
        txtLastname.text = lastname
        txtBirthday.text = birthday
        txtAddress.text = address
        txtCity.text = city
        loadImage("http://i261412.iris.fhict.nl/afty/img/user/\(prefs.valueForKey("AVATAR") as! String)", img: userAvatar)
        
    }
    
    func loadImage(urlString:String, img:UIImageView)
    {
        let imgURL: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: imgURL)
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func display_image()
                {
                    img.image = UIImage(data: data!)
                }
                
                dispatch_async(dispatch_get_main_queue(), display_image)
            }
            
        }
        
        task.resume()
    }
    
    @IBAction func changeAvatar(sender: UIButton) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = self
        myPickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        self.presentViewController(myPickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject]){
        userAvatar.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveProfile(sender: AnyObject) {
        //POST NEW DATA TO SERVER & UPLOAD IMAGE
        // init paramters Dictionary
        var parameters = [
            "task": "task",
            "variable1": "var"
        ]
        
        // add addtionial parameters
        let uid:String = String(prefs.valueForKey("UID") as! Int)
        parameters["firstName"] = txtFirstname.text
        parameters["lastName"] = txtLastname.text
        parameters["birthDay"] = txtBirthday.text
        parameters["city"] = txtCity.text
        parameters["address"] = txtAddress.text
        parameters["uid"] = uid
        // example image data
        let image = userAvatar.image
        let imageData = UIImagePNGRepresentation(image!)
        
        
        
        // CREATE AND SEND REQUEST ----------
        
        let urlRequest = urlRequestWithComponents("http://i261412.iris.fhict.nl/afty/update-profile.php", parameters: parameters, imageData: imageData!)
        
        Alamofire.upload(urlRequest.0, data: urlRequest.1)
            .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                print("\(totalBytesWritten) / \(totalBytesExpectedToWrite)")
            }
            .responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                if(JSON["success"] as! Int==0){
                    let message:String = JSON["error_message"] as! String
                    let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                    self.presentViewController(alert, animated: true){}
                } else {
                    self.prefs.setObject(self.txtFirstname.text, forKey: "FIRSTNAME")
                    self.prefs.setObject(self.txtLastname.text, forKey: "LASTNAME")
                    self.prefs.setObject(self.txtBirthday.text, forKey: "BIRTHDAY")
                    self.prefs.setObject(self.txtAddress.text, forKey: "ADDRESS")
                    self.prefs.setObject(JSON["img"], forKey: "AVATAR")
                    self.prefs.setObject(self.txtCity.text, forKey: "CITY")
                    self.prefs.synchronize()
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }

    }
    
    func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, imageData:NSData) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        var mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Disposition: form-data; name=\"file\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        uploadData.appendData(imageData)
        
        // add parameters
        for (key, value) in parameters {
            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }

    
    

}
